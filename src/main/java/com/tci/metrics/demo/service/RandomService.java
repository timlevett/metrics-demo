package com.tci.metrics.demo.service;

import com.google.common.collect.Lists;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.ImmutableTag;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class RandomService {

  private static final Logger LOGGER = LoggerFactory.getLogger(RandomService.class);

  /**
   * {@link Counter} Demo
   */
  private Counter counter = Metrics.counter(
    "metrics.demo.counter",
    Lists.newArrayList(new ImmutableTag("metric", "demo"), new ImmutableTag("task", "1"))
  );

  public void countSomething() {
    counter.increment(); //increment by 1
    counter.increment(100); //increment by 100
  }


  /**
   * {@link Timer} Demo
   */
  private Timer timer = Metrics.timer("metrics.demo.timer");

  /**
   * Time an Arbitrary Block Manually
   */
  public void timeManual() {
    LocalDateTime start = LocalDateTime.now();
    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      LOGGER.error("oh nos!", e);
    }
    LocalDateTime end = LocalDateTime.now();

    timer.record(Duration.between(start, end));
    LOGGER.info("Recorded Duration {}", Duration.between(start, end).getSeconds());
  }


  /**
   * Time a Method via Supplier Semantics
   */
  public void timeSupplier() {
    String someValue = timer.record(() -> randomTiming());
    LOGGER.info("Recorded Random Timing {}", someValue);
  }

  private String randomTiming() {
    int sleep = new Random(1998383L).nextInt() % 10000;

    try {
      Thread.sleep(sleep);
    } catch (InterruptedException e) {
      LOGGER.warn("oh nos!");
    }
    return "slept for " + sleep + " seconds";
  }


  /**
   * {@link Gauge} Demo
   */
  private Map<Integer, String> gaugedMap = Metrics.gaugeMapSize("metrics.demo.gauge", Lists.newArrayList(), new HashMap<>());

  public void gauging(Integer value) {
    for (int i = 0; i < value; i++) {
      gaugedMap.put(i, "value");
    }
  }
}
