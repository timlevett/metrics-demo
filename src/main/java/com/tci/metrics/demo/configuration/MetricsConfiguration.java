package com.tci.metrics.demo.configuration;

import com.google.common.collect.Lists;
import io.micrometer.core.instrument.ImmutableTag;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class MetricsConfiguration {

  private final PrometheusMeterRegistry registry;

  private final Environment environment;

  @Autowired
  public MetricsConfiguration(PrometheusMeterRegistry registry, Environment environment) {
    this.registry = registry;
    this.environment = environment;
  }

  @PostConstruct
  public void addTagsToRegistry() {
    String profiles = Arrays.toString(environment.getActiveProfiles());

    registry
      .config()
      .commonTags(
        Lists.newArrayList(
          new ImmutableTag("sampleApp", "true"),
          new ImmutableTag("spring-profiles", profiles)
        )
      );
  }
}
