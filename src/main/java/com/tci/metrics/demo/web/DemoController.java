package com.tci.metrics.demo.web;

import com.tci.metrics.demo.service.RandomService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Timed
public class DemoController {

  private Long count = 1L;

  private final RandomService service;

  @Autowired
  public DemoController(RandomService service) {
    this.service = service;
  }

  @RequestMapping("/demo")
  public String demo() {
    return (count++).toString();
  }

  @RequestMapping("/counter")
  public void counter() {
    service.countSomething();
  }

  @RequestMapping("/timer")
  public void timer() {
    service.timeManual();
    service.timeSupplier();
  }

  @RequestMapping("/gauge/{value}")
  public void gauge(@PathVariable Integer value) {
    service.gauging(value);
  }
}
