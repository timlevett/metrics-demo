FROM openjdk:8u131-jre-alpine
MAINTAINER Tim Levett <tlevett@trafficcast.com>

RUN apk update && apk add bash && apk add curl
ENV JAVA_OPTS              ""
ENV SPRING_PROFILES_ACTIVE docker
WORKDIR /app

CMD ["/bin/bash", "-c", "java $JAVA_OPTS -jar /app/app.jar --spring.profiles.active=$SPRING_PROFILES_ACTIVE"]

HEALTHCHECK CMD curl -f http://localhost:8080/actuator/health || exit 1

COPY target/app.jar /app/app.jar
