pipeline {
  agent  {
    label 'docker-builder'
  }
  parameters {
    string(defaultValue: "latest", description: 'Docker Image tag, used by docker build', name: 'TAG')
  }
  environment {
    // Project Repository Name - this become the image name as well
    REPO_NAME = "${env.GIT_URL}".replaceAll('git@github.com:trafficcast/', '').replaceAll('.git', '')
    // ECR URL
    ECR="185909278193.dkr.ecr.us-west-2.amazonaws.com"
  }
  stages {
    stage('checkout project') {
      when {
        expression { params.TAG != 'latest' }
      }
      steps {
        checkout([$class: 'GitSCM',
          userRemoteConfigs: [[
            url: "${env.GIT_URL}",
            credentialsId: "${env.CREDENTIALS_HASH}",
            name: 'origin',
          ]],
          branches: [[name: "refs/tags/${params.TAG}"]]
        ])
      }
    }
    stage('Maven Build') {
      steps {
        sh "${tool 'maven3'}/bin/mvn clean package"
      }
    }
    stage('Docker Build') {
      steps {
        sh "docker build -t ${ECR}/${REPO_NAME}:${params.TAG} ."
      }
    }
    stage('Docker Push') {
      steps {
        sh "docker push ${ECR}/${REPO_NAME}:${params.TAG}"
      }
    }
  }
  post {
    failure {
      slackSend botUser: true, channel: 'dynaflow-builds', color: 'danger', message: "Build ${env.JOB_NAME} failed. Build number: ${env.BUILD_NUMBER}  (<${env.BUILD_URL}|Open>)"
    }
  }
}