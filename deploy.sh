#!/usr/bin/env bash

# Build Spring Boot App
mvn package

# Build Docker Image
docker build -t metrics-demo:latest .

# Deploy Docker Stack
docker stack deploy -c docker-stack.yml demo
